import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home'
import Upload from './views/Upload'

Vue.use(Router)

export default new Router({
    routes: [
        { path: '/', name: 'home', component: Home },
        { path: '/upload', component: Upload }
    ]
})
