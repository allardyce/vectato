import json from 'rollup-plugin-json';
import commonjs from 'rollup-plugin-commonjs';
import resolve from 'rollup-plugin-node-resolve';

import pkg from './package.json';

export default [
    {

        input: 'src/index.js',

        output: [
            {
                file: pkg.module,
                format: 'es'
            },
            {
                file: pkg.main,
                format: 'cjs',
                exports: 'named'
            },
        ],

        external: ['canvas', 'events'],

        plugins: [

            json(),

            resolve(),

            commonjs(),

        ],

    }
];