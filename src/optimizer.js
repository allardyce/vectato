import Step from './step';
import { emptySVG, getBinarySize } from './util';
import State from './state';
import Canvas from './canvas';
import { Shape } from './shape'
import { EventEmitter } from 'events';


class Optimizer extends EventEmitter {

    constructor(input, config) {
        super();
        this.input = input;
        this.config = config;
        this.meta = {};
        this.steps = 0;
        this.start();
    }

    async start() {
        if (!this.image) this.image = await Canvas.initialise(this.input, this.config);

        this.ts = Date.now();
        this.state = new State(this.image, Canvas.empty(this.config));
        this.svg = emptySVG(this.config);

        //Autorun
        if (this.config.auto) {
            this.addShape();
        }
    }

    addShape() {

        this.findBestStep()
            .then(step => this.optimizeStep(step))
            .then(step => {

                this.steps++;

                if (step.distance < this.state.distance) { /* better than current state, epic */

                    this.state = step.apply(this.state);

                    this.svg.addChild(step.toSVG());

                    let svgString = this.svg.toString();
                    this.meta.step = this.steps;
                    this.meta.distance = step.distance;
                    this.meta.size = getBinarySize(svgString);
                    this.meta.time = Date.now() - this.ts;

                    this.emit('step', svgString, this.meta);

                }
                if (this.config.auto) {
                    this.continue();
                }
            });
    }

    async continue() {

        if (this.steps < this.config.steps) {

            //Don't block main thread, allows step events to render
            setImmediate(() => this.addShape());

        } else {

            let svgString = this.svg.toString();
            this.meta.step = this.steps;
            this.meta.distance = this.state.target.distance(this.state.canvas);
            this.meta.size = getBinarySize(svgString);
            this.meta.time = Date.now() - this.ts;

            this.emit('finish', svgString, this.meta);

        }
    }

    findBestStep() {

        let steps = [], bestStep = null;

        for (let i = 0; i < this.config.shapes; i++) {

            let shape = Shape.create(this.config);

            let step = new Step(shape, this.config)
                .compute(this.state)
                .then(step => {
                    if (!bestStep || step.distance < bestStep.distance) {
                        bestStep = step;
                    }
                });

            steps.push(step);

        }

        return Promise.all(steps).then(() => bestStep);

    }

    optimizeStep(step) {

        return new Promise(resolve => {

            let failedAttempts = 0;

            const tryMutation = () => {

                if (failedAttempts >= this.config.mutations) {
                    return resolve(step);
                }

                step.mutate()
                    .compute(this.state)
                    .then(mutatedStep => {
                        if (mutatedStep.distance < step.distance) {
                            failedAttempts = 0;
                            step = mutatedStep;
                        } else {
                            failedAttempts++;
                        }
                        tryMutation();
                    });
            }

            tryMutation();

        });

    }
}


export default Optimizer