import Optimizer from './optimizer';

const defaultConfig = {
    auto: true,
    computeSize: 256,
    viewSize: 512,
    steps: 10,
    shapes: 200,
    alpha: 0.5,
    mutations: 10,
    mutateAlpha: true,
    shapeTypes: ['triangle'],
    fill: 'auto',
    width: null,
    height: null,
    scale: 2,
}

const create = (input, config = {}) => {
    config = { ...defaultConfig, ...config };
    return new Optimizer(input, config);
}

const trace = (input, config = {}) => {
    config = { ...defaultConfig, ...config };
    return new Promise((resolve, reject) => {
        const vectato = new Optimizer(input, config);
        vectato.on('finish', resolve);
    });
}

export default { create, trace }