import { difference, differenceToDistance, getFill, getScale } from './util';
const { createCanvas, loadImage } = require('canvas');



/* Canvas: a wrapper around a <canvas> element */
class Canvas {
	
	constructor(width, height) {
		this.node = createCanvas(width, height);
		this.ctx = this.node.getContext('2d');
		this.imageData = null;
	}

	static empty(config) {
		return new this(config.width, config.height).fill(config.fill);
	}

	static async initialise(image, config) {

		let img = await loadImage(image);

		let computeScale = getScale(img.width, img.height, config.computeSize);
		config.width = img.width / computeScale;
		config.height = img.height / computeScale;

		let viewScale = getScale(img.width, img.height, config.viewSize);
		config.scale = computeScale / viewScale;

		let canvas = this.empty(config);
		canvas.ctx.drawImage(img, 0, 0, config.width, config.height);

		if (config.fill == 'auto') {
			config.fill = getFill(canvas);
		}

		return canvas;

	}


	clone() {
		let otherCanvas = new this.constructor(this.node.width, this.node.height);
		otherCanvas.ctx.drawImage(this.node, 0, 0);
		return otherCanvas;
	}

	fill(color) {
		this.ctx.fillStyle = color;
		this.ctx.fillRect(0, 0, this.node.width, this.node.height);
		return this;
	}

	getImageData() {
		if (!this.imageData) {
			this.imageData = this.ctx.getImageData(0, 0, this.node.width, this.node.height);
		}
		return this.imageData;
	}

	difference(otherCanvas) {
		let data = this.getImageData();
		let dataOther = otherCanvas.getImageData();

		return difference(data, dataOther);
	}

	distance(otherCanvas) {
		let difference = this.difference(otherCanvas);
		return differenceToDistance(difference, this.node.width*this.node.height);
	}

	drawStep(step) {
		this.ctx.globalAlpha = step.alpha;
		this.ctx.fillStyle = step.color;
		step.shape.render(this.ctx);
		return this;
	}
}


export default Canvas;