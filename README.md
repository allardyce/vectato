# @allardyce/vectato

Convert Images to SVG placeholders

Live Demo: [https://allardyce.gitlab.io/vectato/](https://allardyce.gitlab.io/vectato/)

## Install

```
yarn add @allardyce/vectato
```


## Setup

```
import vectato from '@allardyce/vectato'
Vue.use(vectato)
```

## Usage

#### Default
```
<div v-vectato>Hello World!</div>
```